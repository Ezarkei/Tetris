#ifndef TLS_H_
#define TLS_H_

#include <sys/types.h>

#ifdef WIN32
typedef long long int ssize_t;
#endif

typedef struct _sls {
    char *d;
    struct _sls *n, *p;
} sls;

typedef struct _nls {
    ssize_t d;
    struct _nls *n, *p;
} nls;

size_t sln(char const *const);
void pchr(char const);
void pnbr(size_t const);
void pstr(char const *);
char *mrg(char const *, char const *);
char *mrgf(char *, char const *);
void prrr(char const *, int const);
void prrrf(char *, int const);
void *nw(size_t const);
void dlt(void *);
void srnd();
int rnd(int const b, int const e);
sls *slsdd(sls *, char *);
sls *slsrm(sls *);
void slsdlt(sls *);
void slsdltf(sls *);
void slsprntr(sls const *);
void slsprntl(sls const *);
nls *nlsdd(nls *, ssize_t);
nls *nlsrm(nls *);
void nlsdlt(nls *);
void nlsprntr(nls const *);
void nlsprntbl(nls const *);

#endif
