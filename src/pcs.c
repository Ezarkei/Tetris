#ifdef WIN32
#include <windows.h>
#include <io.h>
#else
#include <unistd.h>
#include <dirent.h>
#endif
#include <fcntl.h>

#include "tls.h"

#include "pcs.h"

pc *cppc(bmp p) {
    pc *n = nw(sizeof(pc));
    n->bl = p;
    n->rn = E_RNT_N;
    n->cl = NULL;
    return (n);
}

void dltcppc(pc *p) {
    dlt(p);
}

static void dltpc(bmp p) {
    dbmp(p);
}

static bmp ldpcsz(int const f, char const *p) {
    bool w = 1;
    char b, m = -1;
    size_t x = 0, y = 0, t = 0;
    while (w)
        if (1 > read(f, &b, 1)) {
            y = t > y ? t : y;
            if (x && y)
                w = 0;
            else
                prrrf(mrg("piece preview :: parsing error :: ", p), 132);
        } else if (32 == b || 48 == b || 49 == b || 64 == b) {
            if (!t)
                ++x;
            ++t;
            if (-1 == m)
                m = 48 == b || 49 == b ? 1 : 2;
            else if ((2 == m && (48 == b || 49 == b)) || (1 == m && (32 == b || 64 == b)))
                prrrf(mrg("piece preview :: mixed input :: ", p), 132);
        } else if (10 == b || 13 == b) {
            y = t > y ? t : y;
            t = 0;
        } else
            prrrf(mrg("piece preview :: wrong input :: ", p), 132);
    crd c = {x, y};
    return (nbmp(c, 1));
}

static void ldpcdt(int const f, bmp n) {
    bool w = 1;
    char b;
    crd c = {0, 0};
    while (w)
        if (1 > read(f, &b, 1)) {
            w = 0;
        } else if (10 == b || 13 == b) {
            ++c.x;
            c.y = 0;
        } else {
            if (49 == b || 64 == b)
                bmpstv(n, c, 0, 1);
            ++c.y;
        }
}

static bmp ldpc(char const *const p) {
    int const f = open(p, O_RDONLY);
    if (0 > f)
        prrrf(mrg("load piece :: cannot open file :: ", p), 132);
    bmp n = ldpcsz(f, p);
    lseek(f, 0, SEEK_SET);
    ldpcdt(f, n);
    close(f);
    return (n);
}

static crd crdrv(crd c) {
    c.x ^= c.y;
    c.y ^= c.x;
    c.x ^= c.y;
    return (c);
}

crd pcgtcrd(pc const *p) {
    crd c = bmpgtcrd(p->bl);
    return (p->rn % 2 ? crdrv(c) : c);
}

crd pccvtcrd(pc const *p, crd i) {
    crd const c = pcgtcrd(p);
    i.x = E_RNT_W == p->rn || E_RNT_S == p->rn ? c.x - 1 - i.x : i.x;
    i.y = E_RNT_E == p->rn || E_RNT_S == p->rn ? c.y - 1 - i.y : i.y;
    return (E_RNT_E == p->rn || E_RNT_W == p->rn ? crdrv(i) : i);
}

void prntpc(pc const *p) {
    crd const c = pcgtcrd(p);
    for (crd i = {0, 0}; c.x > i.x; ++i.x) {
        for (i.y = 0; c.y > i.y; ++i.y)
            pchr(bmpgtv(p->bl, pccvtcrd(p, i), 0) ? 64 : 32);
        pchr(10);
    }
}

static pcls *pclscrt(pc *d) {
    pcls *n = nw(sizeof(pcls));
    n->d = d;
    n->n = n->p = n;
    return (n);
}

pcls *pclsdd(pcls *l, pc *d) {
    pcls *n = pclscrt(d);
    if (!l)
        return (n);
    n->n = l;
    n->p = l->p;
    l->p->n = n;
    l->p = n;
    return (l);
}

pcls *pclsrm(pcls *l) {
    pcls *p = l == l->n ? NULL : l->n;
    l->n->p = l->p;
    l->p->n = l->n;
    dlt(l);
    return (p);
}

pcls *pclsrmf(pcls *l) {
    pcls *p = l == l->n ? NULL : l->n;
    l->n->p = l->p;
    l->p->n = l->n;
    dlt(l->d);       dlt(l);
    return (p);
}

void pclsdlt(pcls *l) {
    if (!l)
        return;
    pcls *i = l, *b;
    do {
        b = i;
        i = i->n;
        dlt(b);
    } while (i != l);
}

void pclsdltf(pcls *l) {
    if (!l)
        return;
    pcls *i = l, *b;
    do {
        b = i;
        i = i->n;
        dlt(b->d);
        dlt(b);
    } while (i != l);
}

void pclsprntr(pcls const *l) {
    if (!l)
        return;
    pcls const *i = l;
    do {
        prntpc(i->d);
        i = i->n;
        if (i != l)
            pchr(10);
    } while (i != l);
}

void pclsprntl(pcls const *l) {
    if (!l)
        return;
    pcls const *i = l = l->p;
    do {
        prntpc(i->d);
        i = i->p;
        if (i != l)
            pchr(10);
    } while (i != l);
}

static pcs *crtpcs(size_t const s, sls *n) {
    pcs *p = nw(sizeof(pcs));
    p->sz = s;
    p->pcs = nw(sizeof(bmp) * s);
    for (size_t k = 0; s > k; ++k) {
        k[p->pcs] = ldpc(n->d);
        n = n->n;
    }
    return (p);
}

pcs *ldpcs(char const *const f) {
    sls *n = NULL;
    size_t k = 0;

#ifdef WIN32
    char* s = mrg(f, 47 != (sln(f) - 1)[f] ? "/cfg/pcs/*" : "cfg/pcs/*");
    HANDLE d;
    WIN32_FIND_DATA i;
    for (char* m = s; *m; ++m)        if (47 == *m)
            *m += 45;
    if (INVALID_HANDLE_VALUE == (d = FindFirstFileA(s, &i)))
#else
        char* s = mrg(f, 47 != (sln(f) - 1)[f] ? "/cfg/pcs/" : "cfg/pcs/");
    DIR* d;
    struct dirent* i;
    if (!(d = opendir(s)))
#endif
        prrrf(mrg("loading pieces :: opendir error :: ", s), 132);
#ifdef WIN32
    (sln(s) - 1)[s] = 0;
    do {
        if (4 < sln(i.cFileName)) {
            char const* e = ".pcs", * r = i.cFileName + sln(i.cFileName) - 4;
#else
            while ((i = readdir(d)))
                if (8 == i->d_type && 4 < sln(i->d_name)) {
                    char const* e = ".pcs", * r = i->d_name + sln(i->d_name) - 4;
#endif
                    bool g = 1;
                    for (unsigned char j = 4; j && g; --j)
                        if (*e++ != *r++)
                            g = 0;
                    if (g) {
#ifdef WIN32
                        char* l = mrg(s, i.cFileName);
#else
                        char* l = mrg(s, i->d_name);
#endif
                        n = slsdd(n, l);
                        ++k;
                    }
#ifdef WIN32
                }
        } while (FindNextFile(d, &i));
        FindClose(d);
#else
    }
    closedir(d);
#endif
    if (!n)
        prrrf(mrg("loading pieces :: no pieces found :: ", s), 132);
    dlt(s);
    pcs *p = crtpcs(k, n);
    slsdltf(n);
    return (p);
}

void dltpcs(pcs *p) {
    for (size_t i = 0; p->sz > i; ++i)
        dltpc(i[p->pcs]);
    dlt(p->pcs);
    dlt(p);
}

void prntpcs(pcs const *p) {
    for (size_t i = 0; p->sz > i; ++i) {
        pbmp(i[p->pcs]);
        if (p->sz > i + 1)
            pchr(10);
    }
}
