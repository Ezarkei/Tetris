#ifndef SFML_H_
#define SFML_H_

#include <SFML/Audio.h>
#include <SFML/Graphics.h>

#include "vctr.h"

typedef enum _sprt {
    E_SPRT_I_SM,
    E_SPRT_I_SU,
    E_SPRT_I_SR,
    E_SPRT_I_SD,
    E_SPRT_I_SL,
    E_SPRT_I_SFT,
    E_SPRT_I_SFS,
    E_SPRT_F_SU = 0x1 << E_SPRT_I_SU,
    E_SPRT_F_SR = 0x1 << E_SPRT_I_SR,
    E_SPRT_F_SD = 0x1 << E_SPRT_I_SD,
    E_SPRT_F_SL = 0x1 << E_SPRT_I_SL,
    E_SPRT_F_SFT = 0x1 << E_SPRT_I_SFT,
    E_SPRT_F_SFS = 0x1 << E_SPRT_I_SFS
} sprt;

typedef struct _sfml {
    sfRenderWindow *wn;
    sfTexture *tx;
    sfFont *fn;
    sfMusic *ms;
    sfEvent vnt;
    sfSprite *sp[7];
    sfText *scr, *mtd, *ctr;
    char *bf, *mtdb, *ctrl;
    float vl;
    sfClock *tmr;
} sfml;

sfml *crtsfml(char const *, vctr const, float const, size_t const);
void dltsfml(sfml *);

#endif
