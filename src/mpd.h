#ifndef MPD_H_
#define MPD_H_

#include "rnt.h"
#include "clrs.h"
#include "bmp.h"

typedef struct _mpd {
    bmp bl;
    rnt *rn;
    clr **cl;
    size_t zm;
} mpd;

mpd *crtmpd(char const *);
void dltmpd(mpd *);
void prntmpd(mpd *);

#endif
