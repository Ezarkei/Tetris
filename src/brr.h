#ifndef BRR_H_
#define BRR_H_

#include <stdbool.h>

typedef unsigned char trtr;
typedef char *brr;
typedef char const *brrc;

bool brrgt(brrc const, trtr const);
void brrst(brr const, trtr const, bool const);
void pbt(bool const);

#endif
