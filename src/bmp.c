#include <string.h>

#include "tls.h"

#include "bmp.h"

bool bmpgtv(bmpc const a, crd const c, trtr const f) {
    bmpmtdt *m = PTRTBMPMTDT(a);
    unsigned int const i = CRDTOI(c, m->__);
    trtr p = BMPPDR(m->_);
    return (brrgt(a + i / p, i % p * m->_ + f));
}

fld bmpgtfld(bmpc a, crd const c) {
    bmpmtdt *m = PTRTBMPMTDT(a);
    unsigned int const i = CRDTOI(c, m->__);
    trtr p = BMPPDR(m->_);
    fld v = {nw(m->_)};
    a += i / p;
    for (trtr j = 0; j < m->_; ++j)
        j[v._] = brrgt(a, i % p * m->_ + j);
    return (v);
}

void bmpstv(bmp const a, crd const c, trtr const f, bool const v) {
    bmpmtdt *m = PTRTBMPMTDT(a);
    unsigned int const i = CRDTOI(c, m->__);
    trtr p = BMPPDR(m->_);
    brrst(a + i / p, i % p * m->_ + f, v);
}

void bmpstfld(bmp a, crd const c, fld const v) {
    bmpmtdt *m = PTRTBMPMTDT(a);
    unsigned int const i = CRDTOI(c, m->__);
    a += i;
    for (trtr j = 0; j < m->_; ++j)
        brrst(a, i % BMPPDR(m->_) * m->_ + j, j[v._]);
}

void pbmpf(bmpc const a, trtr const b, trtr const e) {
    bmpmtdt *m = PTRTBMPMTDT(a);
    for (crd c = {0, 0}; c.x < m->__.x; ++c.x, pchr(10))
        for (c.y = 0; c.y < m->__.y; m->__.y - 1 > c.y ? (pchr(32), ++c.y) : (c.y |= m->__.y))
            for (trtr i = b; i < e; pbt(bmpgtv(a, c, i++)));
}

void pbmp(bmpc const a) {
    bmpmtdt *m = PTRTBMPMTDT(a);
    pbmpf(a, 0, m->_);
}

bmp nbmp(crd const c, trtr const f) {
    trtr d = BMPFLDS(f);
    unsigned int i = c.x * c.y * d, j = i / 8 + (i % 8 ? 1 : 0);
    bmpmtdt *m = nw(sizeof(bmpmtdt) + j);
    bmp b = (bmp)m + sizeof(bmpmtdt);
    m->__ = c;
    m->_ = d;
    memset(b, 0, j);
    return (b);
}

void dbmp(bmp const a) {
    dlt(PTRTBMPMTDT(a));
}

crd bmpgtcrd(bmpc const a) {
    return (PTRTBMPMTDT(a)->__);
}
