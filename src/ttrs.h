#ifndef TTRS_H_
#define TTRS_H_

#include "pcs.h"
#include "mpd.h"

#define CTRLF ((t->kb ? sfKeyZ : sfKeyW) == sf->vnt.key.code || sfKeySpace == sf->vnt.key.code)
#define CTRLTC (sfKeyE == sf->vnt.key.code || sfKeyUp == sf->vnt.key.code)
#define CTRLTA ((t->kb ? sfKeyA : sfKeyQ) == sf->vnt.key.code)
#define CTRLR (sfKeyD == sf->vnt.key.code || sfKeyRight == sf->vnt.key.code)
#define CTRLL ((t->kb ? sfKeyQ : sfKeyA) == sf->vnt.key.code || sfKeyLeft == sf->vnt.key.code)
#define CTRLD (sfKeyS == sf->vnt.key.code || sfKeyDown == sf->vnt.key.code)

typedef enum _vts {
    E_VTS_R,
    E_VTS_L,
    E_VTS_TC,
    E_VTS_TA,
    E_VTS_D
} vts;

typedef struct _ttrs {
    pcs *pcs;
    clrls *cl;
    mpd *mp;
    pcls *np;
    pc *cr;
    crd ps, ws;
    size_t pts, vl;
    float sc, sp;
    nls *vts;
    unsigned char grp, prp;
    bool kb;
} ttrs;

void runttrs(char const *);

#endif
