#include "tls.h"

#include "sfml.h"

sfml *crtsfml(char const *d, vctr const ws, float const tsc, size_t const vl) {
    char *p = mrg(d, 47 != (sln(d) - 1)[d] ? "/ssts/" : "ssts/");
    sfml *n = nw(sizeof(sfml));
    n->vl = vl;
    n->mtdb = mrg("TET", "RIS");
    n->tmr = sfClock_create();
    char *b;
    sfVideoMode m = {ws.x, ws.y, 32};
    n->wn = sfRenderWindow_create(m, n->mtdb, sfClose, NULL);
    if (!n->wn)
        prrr("sfml :: cannot create window", 132);
    b = mrg(p, "sprts.png");
    n->tx = sfTexture_createFromFile(b, NULL);
    if (!n->tx) {
        b = mrgf("sfml :: cannot load texture :: ", b);
        prrrf(b, 132);
    }
    dlt(b);
    b = mrg(p, "fnt.ttf");
    n->fn = sfFont_createFromFile(b);
    if (!n->fn) {
        b = mrgf("sfml :: cannot load font :: ", b);
        prrrf(b, 132);
    }
    dlt(b);
    b = mrg(p, "ms.ogg");
    n->ms = sfMusic_createFromFile(b);
    if (!n->ms) {
        b = mrgf("sfml :: cannot load music :: ", b);
        prrrf(b, 132);
    }
    dlt(b);
    dlt(p);
    sfVector2f sc = {tsc, tsc};
    sfIntRect dt[] = {{45, 45, 70, 70}, {0, 30, 100, 15}, {0, 45, 15, 100}, {0, 0, 100, 15}, {30, 45, 15, 100}, {0, 15, 100, 15}, {15, 45, 15, 100}};
    for (unsigned short int i = 0; 7 > i; ++i) {
        i[n->sp] = sfSprite_create();
        sfSprite_setTexture(i[n->sp], n->tx, sfTrue);
        sfSprite_setScale(i[n->sp], sc);
        sfSprite_setTextureRect(i[n->sp], i[dt]);
    }
    n->bf = mrg("0", "   pts");
    n->scr = sfText_create();
    sfText_setString(n->scr, n->bf);
    sfText_setFont(n->scr, n->fn);
    n->mtd = sfText_create();
    sfText_setString(n->mtd, n->mtdb);
    sfText_setFont(n->mtd, n->fn);
    n->ctr = sfText_create();
    sfText_setFont(n->ctr, n->fn);
    n->ctrl = NULL;
    return (n);
}

void dltsfml(sfml *s) {
    for (unsigned short int i = 0; 7 > i; ++i)
        sfSprite_destroy(i[s->sp]);
    sfClock_destroy(s->tmr);
    sfText_destroy(s->scr);
    sfText_destroy(s->mtd);
    dlt(s->bf);
        dlt(s->mtdb);
    sfMusic_destroy(s->ms);
    sfFont_destroy(s->fn);
    sfTexture_destroy(s->tx);
    sfRenderWindow_destroy(s->wn);
    dlt(s);
}
