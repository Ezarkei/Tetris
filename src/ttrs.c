#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <fcntl.h>

#include "tls.h"
#include "sfml.h"

#include "ttrs.h"

static pc *pckpc(ttrs *t) {
    size_t nd = rnd(0, t->pcs->sz);
    pc *p = cppc(nd[t->pcs->pcs]);
    p->rn = rnd(0, 4);
    clrls *i = t->cl;
    while (nd--)
        i = i->n;
    p->cl = i->d;
    return (p);
}

static void nxtpc(ttrs *t) {
    dlt(t->cr);
    t->cr = t->np->d;
    t->np = pclsrm(t->np);
    t->np = pclsdd(t->np, pckpc(t));
    crd const c = bmpgtcrd(t->mp->bl), d = pcgtcrd(t->cr);
    t->ps.y = c.y / 2 - d.y / 2;
    t->ps.y = t->ps.y > c.y ? 0 : t->ps.y;
    t->ps.x = (d.x - 1) * -1;
}

static size_t rddt(int const f, char const *p) {
    size_t r = 0;
    char b, v = 0;
    bool w = 1;
    while (w) {
        if (1 > read(f, &b, 1)) {
            if (!v)
                prrrf(mrg("settings config :: parsing error :: ", p), 132);
            w = 0;
        } else if (47 <= b && 58 >= b) {
            r *= 10;
            r += b - 48;
        } else if (32 == b || 10 == b) {
            if (!v)
                prrrf(mrg("settings config :: wrong input :: ", p), 132);
            w = 0;
        } else
            prrrf(mrg("settings config :: wrong input :: ", p), 132);
        v = 1;
    }
    return (r);
}

static ttrs *ttrscrt(char const *p) {
    ttrs *t = nw(sizeof(ttrs));
    t->pcs = ldpcs(p);
    t->cl = ldclrs();
    t->mp = crtmpd(p);
    char *s = mrg(p, 47 != (sln(p) - 1)[p] ? "/cfg/ttrs/sts.ttrs" : "cfg/ttrs/sts.ttrs");
    int const f = open(s, O_RDONLY);
    if (0 > f)
        prrrf(mrg("settings config :: cannot open file :: ", s), 132);
    t->vl = rddt(f, s);
    t->kb = rddt(f, s);
    t->grp = rddt(f, s);
    t->prp = rddt(f, s);
    dlt(s);
    close(f);
    t->np = NULL;
    t->vts = NULL;
    for (unsigned char i = 5; i; --i)
        t->np = pclsdd(t->np, pckpc(t));
    t->cr = NULL;
    nxtpc(t);
    crd const c = bmpgtcrd(t->mp->bl);
    t->sc = t->mp->zm / 100.0;
    t->sp = 5;
    t->ws.x = 100 * t->sc * (c.y + 1 + 12);
    t->ws.y = 100 * t->sc * (c.x + 1);
    t->ws.y = t->ws.y < 100 * t->sc * 30 ? 100 * t->sc * 30 : t->ws.y;
    t->pts = 0;
    return (t);
}

static void ttrsdlt(ttrs *t) {
    dltmpd(t->mp);
    dltpcs(t->pcs);
    clrlsdltf(t->cl);
    pclsdltf(t->np);
    nlsdlt(t->vts);
    dlt(t->cr);
    dlt(t);
}

void prntmp(ttrs *t) {
    crd const c = bmpgtcrd(t->mp->bl), d = pcgtcrd(t->cr);
    for (size_t i = c.y + 2; i; --i)
        pchr('-');
    pchr(10);
    for (crd i = {0, 0}, j; c.x > i.x; ++i.x) {
        pchr('|');
        for (i.y = 0; c.y > i.y; ++i.y) {
            if (i.x >= t->ps.x && i.x < t->ps.x + d.x && i.y >= t->ps.y && i.y < t->ps.y + d.y) {
                j.x = i.x - t->ps.x;
                j.y = i.y - t->ps.y;
                if (bmpgtv(t->cr->bl, pccvtcrd(t->cr, j), 0))
                    pchr(64);
                else
                    pchr(bmpgtv(t->mp->bl, i, 0) ? 64 : 32);
            } else
                pchr(bmpgtv(t->mp->bl, i, 0) ? 64 : 32);
        }
        pchr('|');
        pchr(10);
    }
    for (size_t i = c.y + 2; i; --i)
        pchr('-');
    pchr(10);
}

static bool cld(ttrs *t, short int const x, short int const y) {
    crd const c = bmpgtcrd(t->mp->bl), d = pcgtcrd(t->cr);
    for (crd i = {0, 0}, j; d.x > i.x; ++i.x)
        for (i.y = 0; d.y > i.y; ++i.y) {
            j = pccvtcrd(t->cr, i);
            if (bmpgtv(t->cr->bl, j, 0)) {
                j.x = i.x + t->ps.x + x;
                j.y = i.y + t->ps.y + y;
                if (0 > j.y || c.y <= j.y)
                    return (1);
                else if (0 <= j.x && (!(0 <= j.y && c.x > j.x && c.y > j.y) || bmpgtv(t->mp->bl, j, 0)))
                    return (1);
            }
        }
    return (0);
}

static rnt nbnd(crd const j, crd const d, bmpc const crbl, rnt const f) {
    return (j.x >= 0 && j.x < d.x && j.y >= 0 && j.y < d.y && bmpgtv(crbl, j, 0) ? 0x1 << (f + 1) : 0);
}

static rnt frnt(crd j, crd const d, pc *cr, bool const f) {
    rnt md = 0;
    --j.x;
    md |= nbnd(!f ? pccvtcrd(cr, j) : j, d, cr->bl, E_RNT_N);
    j.x += 2;
    md |= nbnd(!f ? pccvtcrd(cr, j) : j, d, cr->bl, E_RNT_S);
    --j.x;
    --j.y;
    md |= nbnd(!f ? pccvtcrd(cr, j) : j, d, cr->bl, E_RNT_W);
    j.y += 2;
    md |= nbnd(!f ? pccvtcrd(cr, j) : j, d, cr->bl, E_RNT_E);
    return (md);
}

static void fx(ttrs *t) {
    crd const c = bmpgtcrd(t->mp->bl), d = pcgtcrd(t->cr);
    rnt md;
    for (crd i = {0, 0}, j; d.x > i.x; ++i.x)
        for (i.y = 0; d.y > i.y; ++i.y) {
            j = pccvtcrd(t->cr, i);
            if (bmpgtv(t->cr->bl, pccvtcrd(t->cr, i), 0)) {
                md = frnt(i, bmpgtcrd(t->cr->bl), t->cr, 0);
                j.x = i.x + t->ps.x;
                j.y = i.y + t->ps.y;
                if (0 <= j.x && 0 <= j.y && c.x > j.x && c.y > j.y) {
                    bmpstv(t->mp->bl, j, 0, 1);
                    (j.y * c.x + j.x)[t->mp->cl] = t->cr->cl;
                    (j.y * c.x + j.x)[t->mp->rn] = md;
                }
            }
        }
    nxtpc(t);
}

static void drspc(sfml *sf, crd c, sprt const m, sprt const d, ttrs *t) {
    c.x ^= c.y;
    c.y ^= c.x;
    c.x ^= c.y;
    sfVector2f ps;
    switch (m) {
    case E_SPRT_I_SU:
    {
        ps.x = c.x;
        ps.y = c.y;
        break;
    }
    case E_SPRT_I_SR:
    {
        ps.x = c.x + 85 * t->sc;
        ps.y = c.y;
        break;
    }
    case E_SPRT_I_SD:
    {
        ps.x = c.x;
        ps.y = c.y + 85 * t->sc;
        break;
    }
    case E_SPRT_I_SL:
    {
        ps.x = c.x;
        ps.y = c.y;
        break;
    }
    case E_SPRT_I_SM:
    {
        ps.x = c.x + 15 * t->sc;
        ps.y = c.y + 15 * t->sc;
        break;
    }
    default:
    {
        prrr("critical index error", 132);
        return;
    }
    }
    sfSprite_setPosition(d[sf->sp], ps);
    sfRenderWindow_drawSprite(sf->wn, d[sf->sp], NULL);
}

static void rtt(ttrs *t, bool const n) {
    crd const b = pcgtcrd(t->cr);
    rnt const o = t->cr->rn;
    if (n) {
        if (!t->cr->rn)
            t->cr->rn = E_RNT_W;
        else
            --t->cr->rn;
    } else {
        ++t->cr->rn;
        t->cr->rn %= E_RNT_M;
    }
    crd const a = pcgtcrd(t->cr);
    t->ps.x += b.x / 2 - a.x / 2;
    t->ps.y += b.y / 2 - a.y / 2;
    if (cld(t, 0, 0))
        t->cr->rn = o;
}

static size_t ln(ttrs *t) {
    bool l;
    size_t r = 0;
    crd const c = bmpgtcrd(t->mp->bl);
    for (crd i = {c.x, 0}; 0 < --i.x;) {
        l = 1;
        for (i.y = 0; c.y > i.y; ++i.y)
            l &= bmpgtv(t->mp->bl, i, 0);
        if (l) {
            ++r;
            for (crd k = {i.x, 0}, j; 0 < k.x; --k.x) {
                j.x = k.x - 1;
                for (k.y = 0; c.y > k.y; ++k.y) {
                    j.y = k.y;
                    bmpstv(t->mp->bl, k, 0, bmpgtv(t->mp->bl, j, 0));
                    (k.y * c.x + k.x)[t->mp->cl] = (j.y * c.x + j.x)[t->mp->cl];
                    (k.y * c.x + k.x)[t->mp->rn] = (j.y * c.x + j.x)[t->mp->rn];
                }
            }
            ++i.x;
            }
    }
    return (r ? ++r : 0);
}

static unsigned char lgcl(ttrs *t) {
    if (cld(t, 1, 0)) {
        if (0 > t->ps.x)
            return (1);
        fx(t);
        return (cld(t, 0, 0) ? t->ps.x = -1000, 1 : ln(t));
    } else
        ++t->ps.x;
    return (0);
}

static void hvts(ttrs *t, vts const v) {
    switch (v) {
    case E_VTS_TC:
    {
        rtt(t, 0);
        break;
    }
    case E_VTS_TA:
    {
        rtt(t, 1);
        break;
    }
    case E_VTS_R:
    {
        if (!cld(t, 0, 1))
            ++t->ps.y;
        break;
    }
    case E_VTS_L:
    {
        if (!cld(t, 0, -1))
            --t->ps.y;
        break;
    }
    case E_VTS_D:
    {
        bool dw = 1;
        while (dw) {
            if (cld(t, 1, 0))
                dw = 0;
            else
                ++t->ps.x;
        }
    }
    }
}

static void dvts(ttrs *t) {
    vts v;
    nls *b;
    if (!t->vts)
        return;
    while (t->vts->n != t->vts) {
        v = t->vts->p->d;
        b = t->vts->p;
        nlsrm(t->vts);
        t->vts = b;
        hvts(t, v);
    }
    v = t->vts->p->d;
    nlsrm(t->vts);
    t->vts = NULL;
    hvts(t, v);
}

static void drblk(sfColor cl, sfml *sf, crd c, ttrs *t, rnt const m) {
    if (m & 0x1 << 7)
        cl.a = t->grp;
    c.x *= 100 * t->sc;
    c.y *= 100 * t->sc;
    if (m & 0x1 << 6)
        cl.a = t->prp;
    for (unsigned char i = 0; 7 > i; ++i)
        sfSprite_setColor(i[sf->sp], cl);
    if (m & E_SPRT_F_SU)
        drspc(sf, c, E_SPRT_I_SU, E_SPRT_I_SFT, t);
    if (m & E_SPRT_F_SR)
        drspc(sf, c, E_SPRT_I_SR, E_SPRT_I_SFS, t);
    if (m & E_SPRT_F_SD)
        drspc(sf, c, E_SPRT_I_SD, E_SPRT_I_SFT, t);
    if (m & E_SPRT_F_SL)
        drspc(sf, c, E_SPRT_I_SL, E_SPRT_I_SFS, t);
    if (!(m & 0x1 << 7))
        drspc(sf, c, E_SPRT_I_SM, E_SPRT_I_SM, t);
    if (m & 0x1 << 6) {
        cl.a = 250;
        for (unsigned char i = 0; 7 > i; ++i)
            sfSprite_setColor(i[sf->sp], cl);
    }
    if (!(m & E_SPRT_F_SU))
        drspc(sf, c, E_SPRT_I_SU, E_SPRT_I_SU, t);
    if (!(m & E_SPRT_F_SR))
        drspc(sf, c, E_SPRT_I_SR, E_SPRT_I_SR, t);
    if (!(m & E_SPRT_F_SD))
        drspc(sf, c, E_SPRT_I_SD, E_SPRT_I_SD, t);
    if (!(m & E_SPRT_F_SL))
        drspc(sf, c, E_SPRT_I_SL, E_SPRT_I_SL, t);
}

static void drmp(ttrs *t, sfml *sf) {
    crd const c = bmpgtcrd(t->mp->bl), d = pcgtcrd(t->cr);
    crd prv, csv = t->ps;
    bool dw = 1;
    while (dw) {
        if (cld(t, 1, 0))
            dw = 0;
        else
            ++t->ps.x;
    }
    prv.x = t->ps.x;
    prv.y = t->ps.y;
    t->ps.x = csv.x;
    t->ps.y = csv.y;
    sfColor cl = {225, 225, 225, 250};
    bool drw;
    for (crd i = {0, 0}, j, k; c.x > i.x; ++i.x) {
        if (c.x - 1 != i.x) {
            k.x = i.x + 1;
            k.y = c.y + 1;
            drblk(cl, sf, k, t, E_SPRT_F_SU | E_SPRT_F_SD);
            k.y = 0;
            drblk(cl, sf, k, t, E_SPRT_F_SU | E_SPRT_F_SD);
        }
        for (i.y = 0; c.y > i.y; ++i.y) {
            k.x = 1 + i.x;
            k.y = 1 + i.y;
            drw = 0;
            if (i.x >= t->ps.x && i.x < t->ps.x + d.x && i.y >= t->ps.y && i.y < t->ps.y + d.y) {
                j.x = i.x - t->ps.x;
                j.y = i.y - t->ps.y;
                if (bmpgtv(t->cr->bl, pccvtcrd(t->cr, j), 0)) {
                    cl.r = t->cr->cl->r;
                    cl.g = t->cr->cl->g;
                    cl.b = t->cr->cl->b;
                    --k.x;
                    drblk(cl, sf, k, t, frnt(j, bmpgtcrd(t->cr->bl), t->cr, 0));
                    drw = 1;
                }
            }
            if (!drw && i.x >= prv.x && i.x < prv.x + d.x && i.y >= prv.y && i.y < prv.y + d.y) {
                j.x = i.x - prv.x;
                j.y = i.y - prv.y;
                if (bmpgtv(t->cr->bl, pccvtcrd(t->cr, j), 0)) {
                    drw = 1;
                    cl.r = t->cr->cl->r;
                    cl.g = t->cr->cl->g;
                    cl.b = t->cr->cl->b;
                    --k.x;
                    drblk(cl, sf, k, t, frnt(j, bmpgtcrd(t->cr->bl), t->cr, 0) | (0x1 << 6));
                }
            }
            if (!drw) {
                if (bmpgtv(t->mp->bl, i, 0)) {
                    cl.r = (i.y * c.x + i.x)[t->mp->cl]->r;
                    cl.g = (i.y * c.x + i.x)[t->mp->cl]->g;
                    cl.b = (i.y * c.x + i.x)[t->mp->cl]->b;
                    k.x = i.x;
                    drblk(cl, sf, k, t, (i.y * c.x + i.x)[t->mp->rn]);
                } else {
                    cl.r = 64;
                    cl.g = 127;
                    cl.b = 127;
                    k.x = i.x;
                    drblk(cl, sf, k, t, 0x1 << 7);
                }
            }
            cl.r = 225;
            cl.g = 225;
            cl.b = 225;
        }
    }
    for (crd i = {0, 0}; c.y + 2 > i.y; ++i.y) {
        i.x = 0;
        drblk(cl, sf, i, t, (!i.y ? E_SPRT_F_SD : E_SPRT_F_SL) | (c.y == i.y - 1 ? E_SPRT_F_SD : E_SPRT_F_SR));
        i.x = c.x;
        drblk(cl, sf, i, t, (!i.y ? E_SPRT_F_SU : E_SPRT_F_SL) | (c.y == i.y - 1 ? E_SPRT_F_SU : E_SPRT_F_SR));
    }
    crd pd = pcgtcrd(t->np->d), pr;
    float zm = 1.0 / (float)(pd.x > pd.y ? pd.x : pd.y) * 4.0, org = t->sc;
    t->sc *= zm;
    sfVector2f scv = {t->sc, t->sc};
    sfIntRect dt[] = {{45, 45, 70, 70}, {0, 30, 100, 15}, {0, 45, 15, 100}, {0, 0, 100, 15}, {30, 45, 15, 100}, {0, 15, 100, 15}, {15, 45, 15, 100}};
    for (unsigned short int i = 0; 7 > i; ++i) {
        sfSprite_setScale(i[sf->sp], scv);
        i[dt].top += 145;
        if (0.1 < t->sc)
            sfSprite_setTextureRect(i[sf->sp], i[dt]);
    }
    pd.x = bmpgtcrd(t->np->d->bl).x;
    pd.y = bmpgtcrd(t->np->d->bl).y;
    pr.x = 7;
    pr.y = 4 + c.y;
    for (crd i = {0, 0}, j; pd.x > i.x; ++i.x)
        for (i.y = 0; pd.y > i.y; ++i.y)
            if (bmpgtv(t->np->d->bl, i, 0)) {
                cl.r = t->np->d->cl->r;
                cl.g = t->np->d->cl->g;
                cl.b = t->np->d->cl->b;
                j.x = i.x + pr.x / zm;
                j.y = i.y + pr.y / zm;
                drblk(cl, sf, j, t, frnt(i, bmpgtcrd(t->np->d->bl), t->np->d, 1));
            }
    t->sc = org;
    scv.x = t->sc;
    scv.y = t->sc;
    for (unsigned short int i = 0; 7 > i; ++i) {
        sfSprite_setScale(i[sf->sp], scv);
        i[dt].top -= 145;
        sfSprite_setTextureRect(i[sf->sp], i[dt]);           }
}

static void pdttxt(ttrs *t, sfml *sf) {
    crd const c = bmpgtcrd(t->mp->bl);
    sfVector2f v = {100 * t->sc * (c.y + 3), 100 * t->sc};
    sfText_setPosition(sf->mtd, v);
    sfText_setCharacterSize(sf->mtd, t->sc * 150);
    v.y += 150 * t->sc;
    sfText_setPosition(sf->scr, v);
    sfText_setCharacterSize(sf->scr, t->sc * 150);
    v.y += 1000 * t->sc;
    sfText_setPosition(sf->ctr, v);
    sfText_setCharacterSize(sf->ctr, t->sc * 100);
    dlt(sf->ctrl);
    sf->ctrl = mrg("==   controls   ==\n\n", "RIGHT                D\nLEFT                    ");
    sf->ctrl = mrgf(sf->ctrl, t->kb ? "Q" : "A");
    sf->ctrl = mrgf(sf->ctrl, "\n\nROTATE   >      E\nROTATE   <      ");
    sf->ctrl = mrgf(sf->ctrl, t->kb ? "A" : "Q");
    sf->ctrl = mrgf(sf->ctrl, "\n\nDROP                   S\nFASTER             ");
    sf->ctrl = mrgf(sf->ctrl, t->kb ? "Z" : "W");
    sf->ctrl = mrgf(sf->ctrl, "\n\nZOOM   +             P\nZOOM    -             M\nRESET                 ESC");
    sfText_setString(sf->ctr, sf->ctrl);
}

static void pdtwnd(ttrs *t, sfml *sf) {
    sfVector2f scv = {t->sc, t->sc};
    for (unsigned short int i = 0; 7 > i; ++i)
        sfSprite_setScale(i[sf->sp], scv);
    crd const c = bmpgtcrd(t->mp->bl);
    t->ws.x = 100 * t->sc * (c.y + 1 + 12);
    t->ws.y = 100 * t->sc * (c.x + 1);
    t->ws.y = t->ws.y < 100 * t->sc * 30 ? 100 * t->sc * 30 : t->ws.y;
    sfRenderWindow_destroy(sf->wn);
    sfVideoMode m = {t->ws.x, t->ws.y, 32};
    sf->wn = sfRenderWindow_create(m, sf->mtdb, sfClose, NULL);
    if (!sf->wn)
        prrr("sfml :: cannot re create window", 132);
    pdttxt(t, sf);
}

static void pdtpt(ttrs *t, sfml *sf) {
    dlt(sf->bf);
    size_t ta = t->pts, tb = 2;
    while (ta /= 10)
        ++tb;
    sf->bf = nw(sizeof(char) * tb);
    ta = t->pts;
    (--tb)[sf->bf] = 0;
    while (tb--) {
        tb[sf->bf] = ta % 10 + 48;
        ta /= 10;
    }
    sf->bf = mrgf(sf->bf, "   pts");
    sfText_setString(sf->scr, sf->bf);
}

static void rwndmp(mpd *n) {
    n->zm = 22;
    crd const c = bmpgtcrd(n->bl);
    dbmp(n->bl);
    n->bl = nbmp(c, 1);
    for (size_t i = c.x * c.y; i;) {
        (--i)[n->rn] = E_RNT_M;
        i[n->cl] = NULL;
    }
}

static void rwndt(ttrs *t) {
    rwndmp(t->mp);
    pclsdltf(t->np);
    nlsdlt(t->vts);
    t->np = NULL;
    t->vts = NULL;
    for (unsigned char i = 5; i; --i)
        t->np = pclsdd(t->np, pckpc(t));
    dlt(t->cr);
    t->cr = NULL;
    nxtpc(t);
    t->sp = 5;
    t->pts = 0;
}

static void rwndsf(sfml *n, float const tsc) {
    sfVector2f sc = {tsc, tsc};
    for (unsigned short int i = 0; 7 > i; ++i)
        sfSprite_setScale(i[n->sp], sc);
    dlt(n->bf);
    n->bf = mrg("0", "   pts");
    sfText_setString(n->scr, n->bf);
}

static void rwndms(sfml *sf) {
    sfMusic_setLoop(sf->ms, sfTrue);
    sfMusic_setVolume(sf->ms, sf->vl);
    sfMusic_play(sf->ms);
}

static void runttrsp(ttrs *t, char const *p) {
    char *s = mrg(p, 47 != (sln(p) - 1)[p] ? "/" : "");
    sfml *sf = crtsfml(s, t->ws, t->sc, t->vl);
    rwndms(sf);
    sfTime tmr;
    bool bst = 0;
    vts k[5] = {0, 0, 0, 0, 0};
    sfClock *cl[5] = {sfClock_create(), sfClock_create(), sfClock_create(), sfClock_create(), sfClock_create()};
    unsigned char w = 0;
    pdttxt(t, sf);
    while (sfRenderWindow_isOpen(sf->wn)) {
        while (sfRenderWindow_pollEvent(sf->wn, &sf->vnt)) {
            if (sf->vnt.type == sfEvtClosed)
                sfRenderWindow_close(sf->wn);
            else if (sf->vnt.type == sfEvtMouseWheelMoved) {
                t->sc += 0.02 * (int)sf->vnt.mouseWheel.delta;
                t->sc = t->sc < 0.02 ? 0.02 : t->sc;
                pdtwnd(t, sf);
            } else if (sf->vnt.type == sfEvtKeyPressed) {
                if (CTRLF && !bst)
                    bst = 1;
                else if (CTRLD && (!E_VTS_D[k] || 200 < sfClock_getElapsedTime(E_VTS_D[cl]).microseconds)) {
                    E_VTS_D[k] = 1;
                    sfClock_restart(E_VTS_D[cl]);
                    t->vts = nlsdd(t->vts, E_VTS_D);                                   } else if (CTRLR && (!E_VTS_R[k] || 200 < sfClock_getElapsedTime(E_VTS_R[cl]).microseconds)) {
                    E_VTS_R[k] = 1;
                    sfClock_restart(E_VTS_R[cl]);
                    t->vts = nlsdd(t->vts, E_VTS_R);
                } else if (CTRLTC && (!E_VTS_TC[k] || 200 < sfClock_getElapsedTime(E_VTS_TC[cl]).microseconds)) {
                    E_VTS_TC[k] = 1;
                    sfClock_restart(E_VTS_TC[cl]);
                    t->vts = nlsdd(t->vts, E_VTS_TC);
                } else if (CTRLTA && (!E_VTS_TA[k] || 200 < sfClock_getElapsedTime(E_VTS_TA[cl]).microseconds)) {
                    E_VTS_TA[k] = 1;
                    sfClock_restart(E_VTS_TA[cl]);
                    t->vts = nlsdd(t->vts, E_VTS_TA);
                } else if (CTRLL && (!E_VTS_L[k] || 200 < sfClock_getElapsedTime(E_VTS_L[cl]).microseconds)) {
                    E_VTS_L[k] = 1;
                    sfClock_restart(E_VTS_L[cl]);
                    t->vts = nlsdd(t->vts, E_VTS_L);
                } else if (sfKeyP == sf->vnt.key.code) {
                    t->sc += 0.02;
                    pdtwnd(t, sf);
                } else if (sfKeyM == sf->vnt.key.code) {
                    t->sc -= 0.02;
                    t->sc = t->sc < 0.02 ? 0.02 : t->sc;
                    pdtwnd(t, sf);
                } else if (sfKeyEscape == sf->vnt.key.code) {
                    rwndt(t);
                    rwndsf(sf, t->sc);
                    rwndms(sf);
                    sfClock_restart(sf->tmr);
                    bst = 0;
                    for (unsigned char i = 0; 5 > i; ++i) {
                        i[k] = 0;
                        sfClock_restart(i[cl]);
                    }
                    w = 0;
                    pdttxt(t, sf);
                }
            } else if (sf->vnt.type == sfEvtKeyReleased) {
                if (CTRLF)
                    bst = 0;
                else if (CTRLR)
                    E_VTS_R[k] = 0;
                else if (CTRLTC)
                    E_VTS_TC[k] = 0;
                else if (CTRLTA)
                    E_VTS_TA[k] = 0;
                else if (CTRLL)
                    E_VTS_L[k] = 0;
                else if (CTRLD)
                    E_VTS_D[k] = 0;
            }
        }
        if (!w) {
            tmr = sfClock_getElapsedTime(sf->tmr);
            if (tmr.microseconds > 1000000 / (t->sp * (bst ? 10.0 : 1.0))) {
                dvts(t);
                w = lgcl(t);
                if (1 == w)
                    sfMusic_pause(sf->ms);
                else if (1 < w) {
                    t->pts += w - 1;
                    pdtpt(t, sf);
                    w = 0;
                    t->sp = (t->pts / 10 + 1) * 5;
                }
                sfClock_restart(sf->tmr);
            }
        }
        sfRenderWindow_clear(sf->wn, sfBlack);
        sfRenderWindow_drawText(sf->wn, sf->mtd, NULL);
        sfRenderWindow_drawText(sf->wn, sf->scr, NULL);
        sfRenderWindow_drawText(sf->wn, sf->ctr, NULL);
        drmp(t, sf);
        sfRenderWindow_display(sf->wn);
    }
    for (unsigned char i = 0; 5 > i; ++i)
        sfClock_destroy(i[cl]);
    dltsfml(sf);
    dlt(s);
}

void runttrs(char const *p) {
    srnd();
    ttrs *t = ttrscrt(p);
    runttrsp(t, p);
    ttrsdlt(t);
}
