#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <fcntl.h>

#include "tls.h"

#include "mpd.h"

static size_t gtsz(int const f, char const *p) {
    size_t r = 0;
    char b;
    bool w = 1;
    while (w)
        if (1 > read(f, &b, 1)) {
            if (!r)
                prrrf(mrg("map config :: parsing error :: ", p), 132);
            w = 0;
        } else if (47 <= b && 58 >= b) {
            r *= 10;
            r += b - 48;
        } else if (32 == b || 10 == b) {
            if (!r)
                prrrf(mrg("map config :: wrong input :: ", p), 132);
            w = 0;
        } else
            prrrf(mrg("map config :: wrong input :: ", p), 132);
    return (r);
}

mpd *crtmpd(char const *p) {
    mpd *n = nw(sizeof(mpd));
    char *s = mrg(p, 47 != (sln(p) - 1)[p] ? "/cfg/ttrs/mp.ttrs" : "cfg/ttrs/mp.ttrs");
    int const f = open(s, O_RDONLY);
    if (0 > f)
        prrrf(mrg("map config :: cannot open file :: ", s), 132);
    crd c = {gtsz(f, s), gtsz(f, s)};
    c.x ^= c.y;
    c.y ^= c.x;
    c.x ^= c.y;
    n->zm = 22;
    dlt(s);
    close(f);
    ++c.x;
    n->bl = nbmp(c, 1);
    n->rn = nw(sizeof(rnt) * c.x * c.y);
    n->cl = nw(sizeof(clr *) * c.x * c.y);
    for (size_t i = c.x * c.y; i;) {
        (--i)[n->rn] = E_RNT_M;
        i[n->cl] = NULL;
    }
    return (n);
}

void dltmpd(mpd *m) {
    dbmp(m->bl);
    dlt(m->rn);
    dlt(m->cl);
    dlt(m);
}

void prntmpd(mpd *m) {
    crd const c = bmpgtcrd(m->bl);
    for (crd i = {0, 0}; c.x > i.x; ++i.x) {
        for (i.y = 0; c.y > i.y; ++i.y)
            pchr(bmpgtv(m->bl, i, 0) ? 64 : 32);
        pchr(10);
    }
}
