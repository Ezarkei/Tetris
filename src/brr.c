#include "tls.h"

#include "brr.h"

bool brrgt(brrc const a, trtr const i) {
    return (*a & (1 << i));
}

void brrst(brr const a, trtr const i, bool const v) {
    v ? (*a |= (1 << i)) : (*a &= ~(1 << i));
}

void pbt(bool const b) {
    pchr(b + 48);
}
