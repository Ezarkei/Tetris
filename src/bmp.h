#ifndef BMP_H_
#define BMP_H_

#include "brr.h"
#include "vctr.h"

#define BMPFLDS(F)(1==F||2==F?F:(((F-1)>>2<<2)+(1<<2)))
#define BMPPDR(F)((((F-1)>>3<<3)+(1<<3))/F)
#define CRDTOI(C, A)(C.y * A.x + C.x)
#define PTRTBMPMTDT(A)((bmpmtdt *)((char *)A - sizeof(bmpmtdt)))

typedef vctr crd;

typedef struct _fld {
    bool *_;
} fld;

typedef struct _bmpmtdt {
    trtr _;
    crd __;
} bmpmtdt;

typedef brr bmp;
typedef brrc bmpc;

bool bmpgtv(brrc const, crd const, trtr const);
fld bmpgtfld(brrc, crd const);

void bmpstv(brr const, crd const, trtr const, bool const);
void bmpstfld(brr, crd const, fld const);

void pbmpf(brrc const, trtr const, trtr const);
void pbmp(brrc const);

brr nbmp(crd const, trtr const);
void dbmp(bmp const);
crd bmpgtcrd(bmpc const);

#endif
