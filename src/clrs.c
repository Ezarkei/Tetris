#include <stddef.h>

#include "tls.h"

#include "clrs.h"

void prntclr(clr const *c) {
    pchr(123);
    for (unsigned char j = 0; 3 > j; ++j) {
        pnbr((size_t)(j[(unsigned char *)&(c->r)]));
        if (2 > j) {
            pchr(44);
            pchr(32);
        }
    }
    pchr(125);
}

static clrls *clrlscrt(clr *d) {
    clrls *n = nw(sizeof(clrls));
    n->d = d;
    n->n = n->p = n;
    return (n);
}

clrls *clrlsddrgb(clrls *l, unsigned char const r, unsigned char const g, unsigned char const b) {
    clr *d = nw(sizeof(clr));
    d->r = r;
    d->g = g;
    d->b = b;
    clrls *n = clrlscrt(d);
    if (!l)
        return (n);
    n->n = l;
    n->p = l->p;
    l->p->n = n;
    l->p = n;
    return (l);
}

clrls *clrlsdd(clrls *l, clr *d) {
    clrls *n = clrlscrt(d);
    if (!l)
        return (n);
    n->n = l;
    n->p = l->p;
    l->p->n = n;
    l->p = n;
    return (l);
}

void clrlsrm(clrls *l) {
    l->n->p = l->p;
    l->p->n = l->n;
    dlt(l);
}

void clrlsdlt(clrls *l) {
    if (!l)
        return;
    clrls *i = l, *b;
    do {
        b = i;
        i = i->n;
        dlt(b);
    } while (i != l);
}

void clrlsdltf(clrls *l) {
    if (!l)
        return;
    clrls *i = l, *b;
    do {
        b = i;
        i = i->n;
        dlt(b->d);
        dlt(b);
    } while (i != l);
}

void clrlsprntr(clrls const *l) {
    if (!l)
        return;
    clrls const *i = l;
    do {
        prntclr(i->d);
        i = i->n;
        if (i == l)
            pchr(10);
        else {
            pchr(44);
            pchr(32);
        }
    } while (i != l);
}

void clrlsprntl(clrls const *l) {
    if (!l)
        return;
    clrls const *i = l = l->p;
    do {
        prntclr(i->d);
        i = i->n;
        if (i == l)
            pchr(10);
        else {
            pchr(44);
            pchr(32);
        }
    } while (i != l);
}

clrls *clrlssfl(clrls *l) {
    if (!l)
        return (l);
    clrls *i = l = l->p;
    size_t s = 0;
    do {
        i = i->n;
        ++s;
    } while (i != l);
    s = rnd(0, s);
    while (s--)
        i = i->n;
    return (i);
}

clrls *ldclrs() {
    clrls *l = clrlsddrgb(NULL, GREEN);
    clrlsddrgb(l, YELLOW);
    clrlsddrgb(l, BLUE);
    clrlsddrgb(l, RED);
    clrlsddrgb(l, PURPLE);
    clrlsddrgb(l, PINK);
    clrlsddrgb(l, CYAN);
    clrlsddrgb(l, ORANGE);
    return (l);
}
