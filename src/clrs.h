#ifndef CLRS_H_
#define CLRS_H_

typedef struct _clr {
    unsigned char r, g, b;
} clr;

#define BLUE 0, 64, 255
#define PINK 255, 127, 255
#define RED 255, 64, 64
#define YELLOW 255, 255, 127
#define GREEN 0, 255, 127
#define CYAN 0, 255, 255
#define ORANGE 255, 127, 0
#define PURPLE 127, 0, 255

typedef struct _clrls {
    clr *d;
    struct _clrls *n, *p;
} clrls;

void prntclr(clr const *);
clrls *clrlsddrgb(clrls *, unsigned char const, unsigned char const, unsigned char const);
clrls *clrlsdd(clrls *, clr *);
void clrlsrm(clrls *);
void clrlsdlt(clrls *);
void clrlsdltf(clrls *);
void clrlsprntr(clrls const *);
void clrlsprntl(clrls const *);
clrls *clrlssfl(clrls *);
clrls *ldclrs();

#endif
