#ifndef PCS_H_
#define PCS_H_

#include <stddef.h>

#include "bmp.h"
#include "rnt.h"
#include "clrs.h"

typedef struct _pc {
    bmp bl;
    rnt rn;
    clr *cl;
} pc;

typedef struct _pcs {
    size_t sz;
    bmp *pcs;
} pcs;

typedef struct _pcls {
    pc *d;
    struct _pcls *n, *p;
} pcls;

pc *cppc(bmp p);
void dltcppc(pc *);
crd pcgtcrd(pc const *);
crd pccvtcrd(pc const *, crd);
void prntpc(pc const *);
pcls *pclsdd(pcls *, pc *);
pcls *pclsrm(pcls *);
pcls *pclsrmf(pcls *);
void pclsdlt(pcls *);
void pclsdltf(pcls *);
void pclsprntr(pcls const *);
void pclsprntl(pcls const *);
pcs *ldpcs(char const *const);
void dltpcs(pcs *);
void prntpcs(pcs const *);

#endif
