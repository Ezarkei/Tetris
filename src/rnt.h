#ifndef RNT_H_
#define RNT_H_

typedef enum _rnt {
    E_RNT_N,
    E_RNT_E,
    E_RNT_S,
    E_RNT_W,
    E_RNT_M
} rnt;

#endif
