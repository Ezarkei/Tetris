#ifndef VCTR_H_
#define VCTR_H_

#include <sys/types.h>

typedef struct _vctr {
    ssize_t x, y;
} vctr;

#endif
