#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#include "tls.h"

size_t sln(char const *const s) {
    char const *e = s;
    while (*e)
        e++;
    return (e - s);
}

void pchr(char const c) {
    write(1, &c, 1);
}

static void pnbrr(size_t const n, bool const l) {
    if (n) {
        pnbrr(n / 10, 1);
        pchr(n % 10 + 48);
    } else if (!l)
        pchr(48);
}

void pnbr(size_t const n) {
    pnbrr(n, 0);
}

void pstr(char const *s) {
    while (*s)
        pchr(*s++);
}

char *mrg(char const *f, char const *s) {
    size_t const lf = sln(f), ls = sln(s), l = lf + ls + 1;
    char *n = nw(sizeof(char) * l);
    size_t i = 0;
    for (size_t j = 0; lf > j; ++i, ++j)
        i[n] = j[f];
    for (size_t j = 0; ls > j; ++i, ++j)
        i[n] = j[s];
    (l - 1)[n] = 0;
    return (n);
}

char *mrgf(char *f, char const *s) {
    size_t const lf = sln(f), ls = sln(s), l = lf + ls + 1;
    char *n = nw(sizeof(char) * l);
    size_t i = 0;
    for (size_t j = 0; lf > j; ++i, ++j)
        i[n] = j[f];
    for (size_t j = 0; ls > j; ++i, ++j)
        i[n] = j[s];
    (l - 1)[n] = 0;
    dlt(f);
    return (n);
}

void prrr(char const *s, int const c) {
    char const *h = "Error :: ";
    size_t const l = 11 + sln(s);
    char *m = malloc(sizeof(char) * l);
    if (!m) {
        pstr(h);
        pstr(s);
        pchr(10);
    } else {
        size_t i = 0;
        for (size_t j = 0; 9 > j; ++i, ++j)
            i[m] = j[h];
        for (size_t k = l - 2, j = 0; k > i; ++i, ++j)
            i[m] = j[s];
        (l - 2)[m] = 10;
        (l - 1)[m] = 0;
        pstr(m);
        dlt(m);
    }
    exit(c);
}

void prrrf(char *s, int const c) {
    char const *h = "Error :: ";
    size_t const l = 11 + sln(s);
    char *m = malloc(sizeof(char) * l);
    if (!m) {
        pstr(h);
        pstr(s);
        pchr(10);
    } else {
        size_t i = 0;
        for (size_t j = 0; 9 > j; ++i, ++j)
            i[m] = j[h];
        for (size_t k = l - 2, j = 0; k > i; ++i, ++j)
            i[m] = j[s];
        (l - 2)[m] = 10;
        (l - 1)[m] = 0;
        pstr(m);
        dlt(m);
    }
    dlt(s);
    exit(c);
}

void *nw(size_t const s) {
    void *p = malloc(s);
    if (!p)
        prrr("nw :: malloc :: NULL", 12);
    return (p);
}

void dlt(void *s) {
    free(s);
}

void srnd() {
    void *p = nw(1);
    srand((size_t)time(NULL) ^ (size_t)p ^ (size_t)&srnd ^ getpid());
    dlt(p);
}

int rnd(int const b, int const e) {
    return (rand() % e + b);
}

static sls *slscrt(char *d) {
    sls *n = nw(sizeof(sls));
    n->d = d;
    n->n = n->p = n;
    return (n);
}

sls *slsdd(sls *l, char *d) {
    sls *n = slscrt(d);
    if (!l)
        return (n);
    n->n = l;
    n->p = l->p;
    l->p->n = n;
    l->p = n;
    return (l);
}

sls *slsrm(sls *l) {
    sls *r = l->n;
    l->n->p = l->p;
    l->p->n = l->n;
    dlt(l);
    return (r);
}

void slsdlt(sls *l) {
    if (!l)
        return;
    sls *i = l, *b;
    do {
        b = i;
        i = i->n;
        dlt(b);
    } while (i != l);
}

void slsdltf(sls *l) {
    if (!l)
        return;
    sls *i = l, *b;
    do {
        b = i;
        i = i->n;
        dlt(b->d);
        dlt(b);
    } while (i != l);
}

void slsprntr(sls const *l) {
    if (!l)
        return;
    sls const *i = l;
    do {
        pchr(34);
        pstr(i->d);
        pchr(34);
        i = i->n;
        if (i == l)
            pchr(10);
        else {
            pchr(44);
            pchr(32);
        }
    } while (i != l);
}

void slsprntl(sls const *l) {
    if (!l)
        return;
    sls const *i = l = l->p;
    do {
        pchr(34);
        pstr(i->d);
        pchr(34);
        i = i->n;
        if (i == l)
            pchr(10);
        else {
            pchr(44);
            pchr(32);
        }
    } while (i != l);
}

static nls *nlscrt(ssize_t d) {
    nls *n = nw(sizeof(nls));
    n->d = d;
    n->n = n->p = n;
    return (n);
}

nls *nlsdd(nls *l, ssize_t d) {
    nls *n = nlscrt(d);
    if (!l)
        return (n);
    n->n = l;
    n->p = l->p;
    l->p->n = n;
    l->p = n;
    return (l);
}

nls *nlsrm(nls *l) {
    nls *r = l->n;
    l->n->p = l->p;
    l->p->n = l->n;
    dlt(l);
    return (r);
}

void nlsdlt(nls *l) {
    if (!l)
        return;
    nls *i = l, *b;
    do {
        b = i;
        i = i->n;
        dlt(b);
    } while (i != l);
}

void nlsprntr(nls const *l) {
    if (!l)
        return;
    nls const *i = l;
    do {
        pnbr(i->d);
        i = i->n;
        if (i == l)
            pchr(10);
        else {
            pchr(44);
            pchr(32);
        }
    } while (i != l);
}

void nlsprntl(nls const *l) {
    if (!l)
        return;
    nls const *i = l = l->p;
    do {
        pnbr(i->d);
        i = i->n;
        if (i == l)
            pchr(10);
        else {
            pchr(44);
            pchr(32);
        }
    } while (i != l);
}
